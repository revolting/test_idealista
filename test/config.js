/**
 * Created by Manuel Rebollo on 08/08/2016.
 */

import { jsdom } from 'jsdom'

global.document = jsdom('<!doctype html><html><body></body></html>');
global.window = document.defaultView;
global.navigator = global.window.navigator;
global.Intl = require('intl');
