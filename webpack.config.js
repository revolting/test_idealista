/**
 * Created by Manuel Rebollo on 08/08/2016.
 */
var path = require('path');
var webpack = require('webpack');
//var ExtractTextPlugin = require("extract-text-webpack-plugin");
var config = require('./config.server.js');

var BUILD_DIR = path.resolve(__dirname, './public');
var APP_DIR = path.resolve(__dirname, './app');
var TEST_DIR = path.resolve(__dirname, './test');

module.exports = {
    devtool: "cheap-source-map",
    entry: [
        "webpack-hot-middleware/client?path=http://" + config.host + ":" + config.port + "/__webpack_hmr",
        TEST_DIR + '/test.js',
        APP_DIR + '/index.jsx'
    ],
    styles:[
        "font-awesome/css/font-awesome.css"
    ],
    output: {
        path: BUILD_DIR,
        filename: 'bundle.js',
        publicPath: "http://" + config.host + ":" + config.port + "/app/"
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoErrorsPlugin(),
        new webpack.IgnorePlugin(/vertx/)
        //new ExtractTextPlugin("./assets/stylesheets/bundle.css")
    ],
    resolve: {
        modulesDirectories: ["./", "./app", "./app/", "./assets", "./assets/", 'node_modules', "./public", "./public/"],
        extensions: ['', '.js', '.jsx'],
        alias: {
            'react': path.join(__dirname, 'node_modules', 'react')
        }
    },
    node: {
        console: true,
        __dirname: true
    },
    module: {
        loaders: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                loader: "babel",
                query:
                {
                    presets:['react','es2015']
                }
            },
            {
                test: /\.js$/,
                loaders: ['babel'],
                include: path.join(__dirname, 'app'),
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                loader: "style!css?sourceMap"
            },
            {
                test: /\.less$/,
                loader: "style!css?sourceMap!less?sourceMap"
            },
            {
                test: /\.png$/,
                loader: 'file?name=[path][name].[ext]'
            },
            {
                test: /\.woff2(\?\S*)?$/,
                loader: "url-loader?limit=100000&mimetype=application/font-woff"
            },
            {
                test: /\.woff(\?v=\d+\.\d+\.\d+)?$/,
                loader: "url-loader?limit=100000&mimetype=application/font-woff"
            },
            {
                test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file?name=[path][name].[ext]"
            },
            {
                test: /\.eot(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file?name=[path][name].[ext]"
            },
            {
                test: /\.svg(\?v=\d+\.\d+\.\d+)?$/,
                loader: "file?name=[path][name].[ext]"
            },
            {
                test: /\.json$/,
                loaders: ['json'],
                exclude: /node_modules/,
                include: __dirname
            }
        ]
    }
};
