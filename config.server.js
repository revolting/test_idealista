/**
 * Created by Manuel Rebollo on 08/08/2016.
 */
var path = require('path');
var config = {};

/**
 * Configuracion del proxy del servidor de desarrollo
 */
config.proxy = {
    //Es el servidor adonde va a apuntar el proxy
    target: 'http://localhost:3000',
    //Agrega el servidor en la cabecera del mensaje (request header)
    changeOrigin: true,
    //Nivel de log del proxy ['debug', 'info', 'warn', 'error', 'silent']
    logLevel: "info",
    //Evento que se lanza al recibir un error del proxy (Ej: 404 NOT FOUND)
    onError(err, req, res) {
        res.writeHead(500, {
            'Content-Type': 'application/json'
        });
        res.end('{}');
    }
};
/**
 * Contexto de la aplicacion para el proxy para que se parezca a un entorno real
 */
config.context = '/idealista';
/**
 * Pagina principal que sirve el servidor de desarrollo
 */
config.initPage = path.join(__dirname + '/index.html');
/**
 * Nombre del host del servidor de desarrollo
 * WARNING !! : Si cambias el nombre del host tambien lo tienes que cambiar en estos ficheros:
 * index.html
 */
config.host = "localhost";
/**
 * Puerto en el que el servidor de desarrollo se va a desplegar
 * WARNING !! : Si cambias este puerto tambien lo tienes que cambiar en estos ficheros:
 * index.html
 */
config.port = 3000;

module.exports = config;