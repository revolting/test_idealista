/**
 * Created by Manuel Rebollo on 08/08/2016.
 */
var path = require('path');
var express = require('express');
var webpack = require('webpack');
var proxyMiddleware = require('http-proxy-middleware');
var httpProxy = require('http-proxy');
var config = require('./config.server.js');
var comments = require('./comments.server.js');
var configwp = require('./webpack.config');
var proxyRedirect = httpProxy.createProxyServer({});
var app = express();
//Activador de comentarios.
comments.devServer();

/**
 * Creacion del proxy
 */
var proxy = proxyMiddleware(config.context, config.proxy);
/**
 * Configuracion para el servidor de desarrollo
 */
var compiler = webpack(configwp);
app.use(require('webpack-dev-middleware')(compiler, {
    noInfo: true,
    publicPath: configwp.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

/**
 * Sirve la pagina inicial
 */
app.get(config.context, function (req, res) {
    res.sendFile(config.initPage);
});

/**
 * Ficheros estaticos de la app
 */
app.use('/assets', express.static('assets'));

app.use('/app', express.static('app'));

app.use('/public', express.static('public'));

app.use(proxy);

app.listen(config.port, 'localhost', function (err) {
    if (err) {
        console.log(err);
        return;
    }
});