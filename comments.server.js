/**
 * Created by Manuel Rebollo on 08/08/2016.
 */

var config = require('./config.server.js');
var comments = {};

/**
 * Comentarios para el servidor de desarrollo
 */
comments.devServer = function () {
    console.log("El servidor ha sido arrancado. Puedes entrar por: http://" + config.host + ":" + config.port + "" + config.context);
    console.log("*************************************************************************");
};

module.exports = comments;