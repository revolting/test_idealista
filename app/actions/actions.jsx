/**
 * Created by Manuel Rebollo on 08/08/2016.
 */

import { SET_COMPANIES, SHOW_NOTIFICATION, HIDE_NOTIFICATION } from '../constants/constants.jsx';
import items from "../../assets/resources/listing.json";

/*
 * action creators
 */

export function getAllItems() {
    // Sorting companies to make user easier find an item
    items.sort( function( a, b ) {
        a = a.company.toLowerCase();
        b = b.company.toLowerCase();

        return a < b ? -1 : a > b ? 1 : 0;
    });

    return {
        type: SET_COMPANIES,
        companies: items
    }
}

export function displayNotification(id, company = "") {
    return {
        type: SHOW_NOTIFICATION,
        id: id,
        company: company
    }
}

export function hideNotification() {
    return {
        type: HIDE_NOTIFICATION
    }
}

function setCompanies(companies){
    return {
        type: SET_COMPANIES,
        companies: companies
    }
}

export function removeCompany(id, c) {
    var companies = c.slice();
    for(var i = 0; i < companies.length; i++) {
        if (companies[i].id == id){
            companies.splice(i, 1);
            break;
        }
    }

    return dispatch => {
        dispatch(setCompanies(companies));
        dispatch(hideNotification());
    }
}