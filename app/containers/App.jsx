/**
 * Created by Manuel Rebollo on 08/08/2016.
 */

import '../../assets/stylesheets/container.less';
import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import {displayNotification, removeCompany} from "../actions/actions.jsx"
import Notification from '../components/Notification.jsx'

export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            companies: []
        }
    }

    componentDidMount() {
        this.setState({companies: this.props.companies});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({companies: nextProps.companies});
    }

    parseItem(item){
        return(
            <div className="companyContainer" onClick={this.props.displayNotification.bind(this, item.id, item.company)}>
                <div className="companyContainer-title">
                    <span>{item.company}</span>
                </div>
                <div className="companyContainer-description">
                    <span>{item.description}</span>
                </div>
                <div className="companyContainer-details">
                    <div>
                        <i className="fa fa-envelope-o" aria-hidden="true"></i>
                        <span>{item.email}</span>
                    </div>
                    <div>
                        <i className="fa fa-map-marker" aria-hidden="true"></i>
                        <span>{item.country}</span>
                    </div>
                    <div>
                        <i className="fa fa-share" aria-hidden="true"></i>
                        <span>{item.url}</span>
                    </div>
                </div>
                <div className="companyContainer-remove">
                    <i className="fa fa-times" aria-hidden="true"></i>
                </div>
            </div>
        );
    }

    removeCompany(id){
        this.props.removeCompany(id, this.props.companies);
    }

    render() {
        const companies = this.props.companies;
        var itemsList = [];
        for(var i = 0; i < companies.length; i++) {
            itemsList.push(this.parseItem(companies[i]));
        }
        return (
            <div className="companies">
                {itemsList}
                <Notification acceptAction={this.removeCompany.bind(this)}/>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        companies: state.companies
    }
}

export default connect(mapStateToProps, {displayNotification, removeCompany})(App)