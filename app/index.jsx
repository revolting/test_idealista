/**
 * Created by Manuel Rebollo on 07/08/2016.
 */

import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk'

// Containers para el router
import {getAllItems} from './actions/actions.jsx'
import companyManager from './reducers/reducers.jsx';
import App from './containers/App';

//Creacion del primer state del store
const middleware = [ thunk ];

const store = createStore(
    companyManager,
    applyMiddleware(...middleware)
)

store.dispatch(getAllItems())

render(
    <div>
        <Provider store={store}>
            <App/>
        </Provider>
    </div>,
    document.getElementById('react')
);