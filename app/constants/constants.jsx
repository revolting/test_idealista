/**
 * Created by Manuel Rebollo on 08/08/2016.
 */

export const SHOW_NOTIFICATION = "SHOW_NOTIFICATION";
export const HIDE_NOTIFICATION = "HIDE_NOTIFICATION";
export const SET_COMPANIES = "SET_COMPANIES";