/**
 * Created by Manuel Rebollo on 08/08/2016.
 */

import React from 'react';
import { render } from 'react-dom';
import { connect } from 'react-redux';
import { Modal, Button, Header, Content, Actions, Icon } from 'semantic-react';
import {hideNotification} from "../actions/actions.jsx"
import '../../assets/stylesheets/semantic.css';

class Notification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
                active: false
        }
    }

    componentDidMount() {
            this.setState({active: this.props.active,
                    header: this.props.header,
                    bodyContent: this.props.bodyContent,
                    id: this.props.id});
    }

    componentWillReceiveProps(nextProps) {
        this.setState({active: nextProps.active,
                header: nextProps.header,
                bodyContent: nextProps.bodyContent,
                id: nextProps.id});
    }

    acceptAction(id){
        this.props.acceptAction(id);
    }

    render() {
        return (
            <div>
                <Modal basic onRequestClose={this.props.hideNotification.bind(this)} active={this.state.active}>
                    <Header icon="trash">{this.state.header}</Header>
                    <Content>
                        <p>{this.state.bodyContent}</p>
                    </Content>
                    <Actions>
                        <Button color="green"
                                basic
                                inverted
                                onClick={this.props.hideNotification.bind(this)}
                            >
                            <i className="fa fa-times" aria-hidden="true"/>
                            Cancelar
                        </Button>
                        <Button color="red"
                                inverted
                                onClick={this.acceptAction.bind(this, this.props.id)}
                            >
                            <i className="fa fa-trash" aria-hidden="true"/>
                            Eliminar
                        </Button>
                    </Actions>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        active: state.modal.active,
        bodyContent: state.modal.bodyContent,
        header: state.modal.header,
        id: state.modal.id
    }
}

export default connect(mapStateToProps, {hideNotification})(Notification)
