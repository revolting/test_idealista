/**
 * Created by Manuel Rebollo on 08/08/2016.
 */

import { SET_COMPANIES, SHOW_NOTIFICATION, HIDE_NOTIFICATION } from '../constants/constants.jsx';

const initialState = {
    companies: [],
    modal:{
        active: false,
        bodyContent: "",
        header: "",
        id: -1
    }
};

export default function companyManager (state = initialState, action) {
    switch (action.type) {
        case SET_COMPANIES:
            return (
                {
                    companies: action.companies,
                    modal:{
                        active: state.modal.active,
                        bodyContent: state.modal.bodyContent,
                        header: state.modal.header
                    }
                }
            );
        case SHOW_NOTIFICATION:
            return (
                {
                    companies: state.companies,
                    modal:{
                        active: true,
                        bodyContent: "¿Está seguro de que desea eliminar la compañía "+action.company+" de la lista?",
                        header: "Eliminar compañía",
                        id: action.id
                    }
                }
            );
        case HIDE_NOTIFICATION:
            return ({
                companies: state.companies,
                modal:{
                    active: false,
                    bodyContent: state.modal.company,
                    header: state.modal.header
                }
            });
        default:
            return (state);
    }
}